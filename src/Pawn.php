<?php

class Pawn extends Figure {
    private $isFirstMove = true;

    /**
     * @return bool
     */
    public function isFirstMove() {
        return $this->isFirstMove;
    }

    public function setNotFirstMove() {
        $this->isFirstMove = false;
    }

    public function __toString() {
        return $this->isBlack() ? '♟' : '♙';
    }
}
