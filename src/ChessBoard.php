<?php

class ChessBoard {
    /**
     * @var Figure[]
     */
    private $board = [];
    /**
     * @var bool
     */
    private $doesLastMoveWasBlack = true;

    public function __construct() {
        $xLetters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'];
        $xBlackFigures = [
            new Rook(), new Knight(), new Bishop(), new Queen(),
            new King(), new Bishop(), new Knight(), new Rook()
        ];
        $xWhiteFigures = [
            new Rook(false), new Knight(false), new Bishop(false), new Queen(false),
            new King(false), new Bishop(false), new Knight(false), new Rook(false)
        ];

        // черные фигуры
        foreach ($xLetters as $index => $xLetter) {
            $this->board[$xLetter][1] = $xWhiteFigures[$index];
        }
        // черные пешки
        foreach ($xLetters as $xLetter) {
            $this->board[$xLetter][2] = new Pawn(false);
        }

        // белые фигуры
        foreach ($xLetters as $index => $xLetter) {
            $this->board[$xLetter][8] = $xBlackFigures[$index];
        }
        // белые пешки
        foreach ($xLetters as $xLetter) {
            $this->board[$xLetter][7] = new Pawn();
        }
    }

    /**
     * @param string $move
     * @throws Exception
     */
    public function move($move) {
        if (!preg_match('/^([a-h])(\d)-([a-h])(\d)$/', $move, $match)) {
            throw new \Exception("Incorrect move");
        }

        $xFrom = $match[1];
        $yFrom = $match[2];
        $xTo = $match[3];
        $yTo = $match[4];

        /** @var Figure $from */
        $from = &$this->board[$xFrom][$yFrom];
        if (isset($from)) {
            // черные и белые должны ходить по очереди
            if (($this->doesLastMoveWasBlack && $from->isBlack()) || (!$this->doesLastMoveWasBlack && !$from->isBlack())) {
                throw new Exception("Invalid move {$xFrom}{$yFrom}-{$xTo}{$yTo}");
            }

            // ходит пешка
            if ($from instanceof Pawn) {
                $pawnMove = new PawnMove($from, $xFrom, $yFrom, $xTo, $yTo, $this->board);
                if (!$pawnMove->canMove()) {
                    throw new Exception("Invalid move {$xFrom}{$yFrom}-{$xTo}{$yTo}");
                }
                $from->setNotFirstMove();
            }

            $this->board[$xTo][$yTo] = $from;
            $this->doesLastMoveWasBlack = !$this->doesLastMoveWasBlack;
        }
        $from = null;
    }

    public function dump() {
        for ($y = 8; $y >= 1; $y--) {
            echo "$y ";
            for ($x = 'a'; $x <= 'h'; $x++) {
                if (isset($this->board[$x][$y])) {
                    echo $this->board[$x][$y];
                }
                else {
                    echo '-';
                }
            }
            echo "\n";
        }
        echo "  abcdefgh\n";
    }
}
