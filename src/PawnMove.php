<?php

class PawnMove {
    /**
     * @var bool
     */
    private $canMove = false;

    /**
     * @param Pawn $pawn
     * @param string $xFrom
     * @param int $yFrom
     * @param string $xTo
     * @param int $yTo
     * @param Figure[] $board
     */
    public function __construct(Pawn $pawn, $xFrom, $yFrom, $xTo, $yTo, array $board) {
        // количество клеток хода
        $squaresToMove = 0;
        // ход либо вертикальный, либо диагональный
        $isVerticalMove = true;

        if ($xFrom === $xTo) {
            $squaresToMove = abs($yFrom - $yTo);
        }
        else {
            $isVerticalMove = false;
            $squaresToMove = abs($yTo - $yFrom);
        }

        // ход по прямой
        if ($isVerticalMove) {
            if ($squaresToMove === 1) {
                if (@$board[$xTo][$yTo] instanceof Figure) {
                    return;
                }
            }
            elseif ($squaresToMove === 2) {
                if (!$pawn->isFirstMove()) {
                    return;
                }

                // между отправной и конечной клетками не должно быть фигуры - это вам не конь
                $nextSquare = ( $pawn->isBlack() ) ? @$board[$xFrom][$yFrom - 1] : @$board[$xFrom][$yFrom + 1];
                if ($nextSquare instanceof Figure) {
                    return;
                }
            }
            else {
                return;
            }
        }
        // по диагонали пешка умеет тока жрат в радиусе одной клетки
        else {
            if (1 < $squaresToMove) {
                return;
            }

            if (!(@$board[$xTo][$yTo] instanceof Figure)) {
                return;
            }
        }

        $this->canMove = true;
    }

    public function canMove() {
        return $this->canMove;
    }
}