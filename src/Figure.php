<?php

class Figure {
    private $isBlack;

    public function __construct($isBlack = true) {
        $this->isBlack = $isBlack;
    }

    /**
     * @return bool
     */
    public function isBlack() {
        return $this->isBlack;
    }

    /** @noinspection PhpToStringReturnInspection */
    public function __toString() {
        throw new \Exception("Not implemented");
    }
}
